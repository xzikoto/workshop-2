﻿using System.Collections.Generic;

namespace OlympicGames.Olympics.Contracts
{
    public interface ISprinter
    {
        IDictionary<string, double> PersonalRecords { get; }
    }
}
