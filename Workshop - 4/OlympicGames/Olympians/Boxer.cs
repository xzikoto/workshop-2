﻿using System;
using OlympicGames.Core;
using OlympicGames.Olympics.Contracts;
using OlympicGames.Olympics.Enums;

namespace OlympicGames.Olympians
{
    public class Boxer : Olympian , IBoxer
    {
        public Boxer(string firstName, string lastName, string country, BoxingCategory category, int wins , int loses) : base(firstName , lastName, country)
        {
                this.Category = category;
                this.Wins = wins;
                this.Losses = loses;    
        }

        public BoxingCategory Category { get; set; }

        public int Wins { get; set; }

        public int Losses { get; set; }
        public override string ToString()
        {
            return  base.ToString() + $"Category: {this.Category} \n" +
                $"Wins: {this.Wins} \n" +
                $"Losses: {this.Losses}"; ;

        }
    }
}
