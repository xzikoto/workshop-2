﻿using System;
using System.Collections.Generic;
using OlympicGames.Core;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Olympians
{
    public class Sprinter : Olympian , ISprinter
    {
        public IDictionary<string, double> personalRecords;
        
        public Sprinter(string firstName, string lastName, string country, IDictionary<string, double> personalRecords)
            : base(firstName, lastName, country)
        {
              
              this.PersonalRecords = personalRecords;
            //finish this shit
        }

        public IDictionary<string, double> PersonalRecords
        {
            get; set;
        }

        public override string ToString()
        {
            string records = "";
            
            foreach (var item in  this.PersonalRecords)
            {
                records += item.Key.ToString() + "m" + ": " + item.Value.ToString() + "s" + "\n";
            }

            if (records != null)
            {
                if (records.Length == 0)
                {
                    return base.ToString() + "NO PERSONAL RECORDS SET\n";
                }
                return base.ToString() + "PERSONAL RECORDS:\n" + records;
                
            }


            return null;
        }
    }
}
