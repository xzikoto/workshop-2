﻿using System;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core
{
    public abstract class Olympian : IOlympian
    {
        private string firstName;
        private string lastName;
        private string country;
        
        public  Olympian(string firstName, string lastName,string country) 
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Country = country;
        }

        public virtual string FirstName
        {
            get { return this.firstName; }

            set
            {
                if (value.Length < 2 || value.Length > 20)
                {
                    throw new Exception("First Name shoulb be between 2 and 20 symbols!");
                }
                else
                {
                    this.firstName = value;
                }
            }
        }
         public virtual string LastName
        {
            get { return this.lastName; }

            set
            {
                if (value.Length < 2 || value.Length > 20)
                {
                    throw new Exception("Last Name shoulb be between 2 and 20 symbols!");
                }
                else
                {
                    this.lastName = value;
                }
            }
        }
        public virtual string Country
        {
            get { return this.country; }

            set
            {
                if (value.Length < 3 || value.Length > 25)
                {
                    throw new Exception("Country shoulb be between 2 and 25 symbols!");
                }
                else
                {
                    this.country = value;
                }
            }
        }


        public new virtual string ToString()


        {
            string info = "";
            return info += $"{this.FirstName} {this.LastName} from {this.Country}\n";
                    
        }
    }
}
