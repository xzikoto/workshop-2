﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Providers
{
    public class OlympicCommittee : IOlympicCommittee
    {
        private readonly List<IOlympian> olympians = new List<IOlympian>();


        public IReadOnlyList<IOlympian> Olympians
        {
            get
            {
                return this.olympians;
            }
            private set
            {
                value = this.olympians;
            }
        }


        public void Add(IOlympian olympian)
        {
            if (olympian as IOlympian != null)
            {

                olympians.Add(olympian);
            }
            else
            {
                throw new Exception("Not a valid IOlympian");
            }

        }
        //public string GetLastAddedOlypian()
        //{
        //    return (olympians[olympians.Count - 1]).ToString();
        //}
        //public String GetOlypianInfo(IOlympian olympian)
        //{
        //    string info = olympian.ToString();
        //    return info;
        //}
    }
}
