﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympians;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Commands
{
    public class ListOlympiansCommand : Command
    {
        public ListOlympiansCommand(IList<string> commandLine, IOlympicCommittee committee)
            : base(commandLine, committee)
        {

        }

        public override string Execute()
        {
            string info = "";

            List<IOlympian> list = new List<IOlympian>();
            foreach (var item in this.Committee.Olympians)
            {
                list.Add(item);

            }
            if (this.Committee.Olympians.Count == 0)
            {
                // list.Sort((x, y) => x.FirstName.CompareTo(y.FirstName));
                return "NO OLYMPIANS ADDED";
            }
            if (CommandParameters.Count == 0)
            {
                list.Sort((x, y) => x.FirstName.CompareTo(y.FirstName));
                info = "Sorted by [key: firstname] in [order: asc] \n";
            }
            else if (CommandParameters[0].ToLower() == "firstname" && CommandParameters.Count == 1)
            {
                list.Sort((x, y) => x.FirstName.CompareTo(y.FirstName));
                info = "Sorted by [key: firstname] in [order: asc] \n";
            }
            else if (CommandParameters[0].ToLower() == "lastname" && CommandParameters.Count == 1)
            {
                list.Sort((x, y) => x.LastName.CompareTo(y.LastName));
                info = "Sorted by [key: lastname] in [order: asc] \n";
            }
            else if (CommandParameters[0].ToLower() == "country" && CommandParameters.Count == 1)
            {
                list.Sort((x, y) => x.Country.CompareTo(y.Country));
                info = "Sorted by [key: country] in [order: asc] \n";

            }
            else
            {
                if (CommandParameters[0].ToLower() == "firstname" && CommandParameters[1].ToLower() == "desc")
                {
                    list.Sort((x, y) => x.FirstName.CompareTo(y.FirstName));
                    list.Reverse();
                    info = "Sorted by [key: firstname] in [order: desc] \n";
                }
                else if (CommandParameters[0].ToLower() == "lastname" && CommandParameters[1].ToLower() == "asc")
                {
                    list.Sort((x, y) => x.LastName.CompareTo(y.LastName));
                    info = "Sorted by [key: lastname] in [order: asc] \n";

                }
                else if (CommandParameters[0].ToLower() == "lastname" && CommandParameters[1].ToLower() == "desc")
                {
                    list.Sort((x, y) => x.LastName.CompareTo(y.LastName));
                    
                    list.Reverse();
                    info = "Sorted by [key: lastname] in [order: desc] \n";
                }
                else if (CommandParameters[0].ToLower() == "firstname" && CommandParameters[1].ToLower() == "asc")
                {
                    list.Sort((x, y) => x.FirstName.CompareTo(y.FirstName));

                    info = "Sorted by [key: firstname] in [order: asc] \n";

                }
                else if (CommandParameters[0].ToLower() == "country" && CommandParameters[1].ToLower() == "asc")
                {
                    list.Sort((x, y) => x.Country.CompareTo(y.Country));
                    info = "Sorted by [key: country] in [order: asc] \n";
                }
                else if (CommandParameters[0].ToLower() == "country" && CommandParameters[1].ToLower() == "desc")
                {
                    list.Sort((x, y) => x.Country.CompareTo(y.Country));
                    list.Reverse();
                    info = "Sorted by [key: country] in [order: desc] \n";

                }




            }



            foreach (IOlympian item in list)
            {
                if (item as Boxer != null)
                {
                    info += "BOXER:"+((Boxer)item).ToString() + "\n";
                }
                else if (item as Sprinter != null)
                {
                    info += "SPRINTER:"+((Sprinter)item).ToString() + "\n";
                }
            }
            return info;

        }
    }
}
