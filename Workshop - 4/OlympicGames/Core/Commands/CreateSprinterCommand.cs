﻿using System;
using System.Collections.Generic;
using System.Linq;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympians;

namespace OlympicGames.Core.Commands
{
    public class CreateSprinterCommand : Command
    {
        public CreateSprinterCommand(IList<string> commandLine, IOlympicCommittee committee)
            : base(commandLine, committee)
        {

        }

        public override string Execute()
        {
            if (CommandParameters.Count() < 3)
            {
                throw new ArgumentException("Please provide all parameters.");
            }
            string name = this.CommandParameters[0];
            string lName = this.CommandParameters[1];
            string country = this.CommandParameters[2];
            IDictionary<string, double> dic = new Dictionary<string, double>();
            for (int i = 3; i < this.CommandParameters.Count(); i++)
            {
                var keyValue = this.CommandParameters[i].Split("/");
                dic.Add(keyValue[0], double.Parse(keyValue[1]));
            }
            
            Sprinter sprinter = (Sprinter)(this.Factory.CreateSprinter(name, lName, country, dic));
            this.Committee.Add(sprinter);
            return $"Created Sprinter  \nSprinter:"+sprinter.ToString();
            
        }
    }
}
