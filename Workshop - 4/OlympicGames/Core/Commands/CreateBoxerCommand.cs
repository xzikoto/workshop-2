﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympians;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Commands
{
    public class CreateBoxerCommand : Command
    {
        
        public CreateBoxerCommand(IList<string> commandLine, IOlympicCommittee committee) 
            : base(commandLine, committee)
        {
           
        }
        
        public override string Execute()
        {
            // TODO: Implement this
             
            this.Committee.Add(this.Factory.CreateBoxer(base.CommandParameters[0],
                    base.CommandParameters[1], base.CommandParameters[2],
                    base.CommandParameters[3], Convert.ToInt32(base.CommandParameters[4]),
                    Convert.ToInt32(base.CommandParameters[5])));

           Boxer boxer =  (Boxer)(this.Committee.Olympians[this.Committee.Olympians.Count - 1]);
            return "Created Boxer  \nBOXER: " + boxer.ToString();
          // return (this.Committee.Olympians[this.Committee.Olympians.Count - 1]).ToString();

        }
    }
}
