﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympians;
using OlympicGames.Olympics.Contracts;
using OlympicGames.Olympics.Enums;

namespace OlympicGames.Core.Factories
{
    public class OlympicsFactory : IOlympicsFactory
    {
        
        public IReadOnlyList<IOlympian> Olympians { get; set; }

        

        

        public IOlympian CreateBoxer(string firstName, string lastName, string country, string category, int wins, int losses)
        {
            BoxingCategory cat = BoxingCategory.Featherweight;
            if (category.ToLower() == BoxingCategory.Featherweight.ToString().ToLower())
            {
                cat = BoxingCategory.Featherweight;
            }
            if (category.ToLower() == BoxingCategory.Flyweight.ToString().ToLower())
            {
                cat = BoxingCategory.Flyweight;
            }
            if (category.ToLower() == BoxingCategory.Heavyweight.ToString().ToLower())
            {
                cat = BoxingCategory.Heavyweight;
            }
            if (category.ToLower() == BoxingCategory.Lightweight.ToString().ToLower())
            {
                cat = BoxingCategory.Lightweight;
            }
            if (category.ToLower() == BoxingCategory.Middleweight.ToString().ToLower())
            {
                cat = BoxingCategory.Middleweight;
            }
            Boxer boxer = new Boxer(firstName, lastName, country,cat, wins, losses);
            if (boxer as IBoxer != null)
            {
                if (boxer as IOlympian != null)
                {
                    return boxer;
                }
                else
                {
                    return null;
                }
                
                
            }
            else
            {
                throw new Exception("Boxer is not IBOXER");
            }
            
        }
       
        public IOlympian CreateSprinter(string firstName, string lastName, string country, IDictionary<string, double> records)
        {
            Sprinter sprinter = new Sprinter(firstName, lastName, country,records);
            if (sprinter as ISprinter != null)
            {
                if (sprinter as IOlympian != null)
                {
                   
                    return sprinter;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                
                throw new Exception("Boxer is not IBOXER");
              
            }
        }


        
    }
}
