﻿using ClassesII_Demo.Models;
using System;

namespace ClassesII_Demo.Infrastructure
{
    public static class Engine
    {
        public static void Run()
        {
            Phonebook.SeedContacts();
            Console.WriteLine( $"Available Commands \n " +
                    $"     1. Type addcontact <name> <phonenumber> \n " +
                    $"     2. Type removecontact <name> <phonenumber \n>" +
                    $"     3. Type listcontacts  listcontacts name  listcontacts time \n" +
                    $"      4. Type updatecontact <Newname> <Newphonenumber> \n" +
                    $"      5. Type serialize all (save all objects in a json file)\n" +
                    $"      6. Type deserialize to load all the object - still not implemented");
            while (true)
            {
                // input
                var input = Console.ReadLine();

                // process
                try
                {
                    var result = CommandProcessor.ProcessCommand(input);
                    Console.WriteLine(result);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
