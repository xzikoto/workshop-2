﻿using ClassesII_Demo.Commands;
using ClassesII_Demo.Models;
using System;

namespace ClassesII_Demo.Infrastructure
{
    public static class CommandProcessor 
    {
        

        public static string ProcessCommand(string input)
        {
            var args = input.Split();
            switch (args[0])
            {
                case "addcontact":

                    var command = new AddContactCommand();
                    return command.Execute(args);
                case "listcontacts":
                    var command2 = new ListContactsCommand();
                    return Phonebook.ListContacts();
               case "updatecontact":
                   var command3 = new UpdateContactCommand();
                   return command3.Execute(args);
                case "removecontact":
                    var command4 = new RemoveContactCommand();
                    return command4.Execute(args);
                case "serialize":
                    var command5 = new SerializeObjectsCommand();
                    return command5.Execute(args);
                case "help":
                    var command6 = new HelpCommands();
                    return command6.Execute(args);
                default:
                    return "Invalid command";
            }
        }

        
    }
}
