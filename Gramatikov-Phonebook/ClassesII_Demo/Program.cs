﻿using ClassesII_Demo.Infrastructure;
using ClassesII_Demo.Commands;
using ClassesII_Demo.Models;
using System;

namespace ClassesII_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Engine.Run();
           
        }
    }
}
