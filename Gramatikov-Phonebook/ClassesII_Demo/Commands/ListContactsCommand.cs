﻿using System;
using ClassesII_Demo.Models;
namespace ClassesII_Demo.Commands
{
    public class ListContactsCommand
    {
        public ListContactsCommand()
        {
        }
        public string Execute(string[] args)
        {
            if (args.ToString() == ("name"))
            {
               return Phonebook.ListContactsName();
            }
            else if (args.ToString() == ("time"))
            {
               return Phonebook.ListContactsTime();
            }
            else if (args.ToString() == ("normal"))
            {
                return Phonebook.ListContacts();
            }
            else
            {
                return $"There are no contacts in the Phonebook. Add some";
            }
            
            
        }
    }
}
