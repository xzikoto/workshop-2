﻿using System;
using ClassesII_Demo.Models;
namespace ClassesII_Demo.Commands
{
    public class RemoveContactCommand
    {
        public RemoveContactCommand()
        {
        }
        public string Execute(string[] args)
        {
            if (args[0] != "removecontact")
            {
                throw new ArgumentException("Please provide at least 2 arguments. The first argument should be name, the second - phonenumber");
            }
            Phonebook.RemoveContact(Phonebook.GetContact(args[1], args[2]));
            return $"Contact REMOVED ";
        }
    }
}
