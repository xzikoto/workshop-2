﻿using System;
using ClassesII_Demo.Models;

namespace ClassesII_Demo.Commands
{
    public class AddContactCommand
    {
        public AddContactCommand()
        {
            

        }
        public string Execute(string[] args)
        {
            if (args.Length != 3)
            {
                throw new ArgumentException("Please provide at least  arguments. The first argument should be name, the second - phonenumber");
            }

            var contact = new Contact(args[1], args[2]);
            Phonebook.AddContact(contact);
            return $"Created contact " + contact.GetContactInfo();
        }
    }
}
