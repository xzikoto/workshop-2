﻿using System;
using ClassesII_Demo.Models;
namespace ClassesII_Demo.Commands
{
    public class UpdateContactCommand
    {
        public UpdateContactCommand()
        {
        }
        public string Execute(string[] args)
        {   
            if (args[0] != "updatecontact")
            {
                throw new ArgumentException("Please provide at least 2 arguments. The first argument should be name, the second - phonenumber");
            }

            Contact contact = null ;
            Phonebook.UpdateContact(args[1], args[2]).ToString();
            contact = (Contact)(Phonebook.GetContact(args[1], args[2]));
           // contact = Phonebook.GetContact(args[1], args[2]);

            return $"Contact UPDATED " + contact.GetContactInfo();
        }
    }
}
