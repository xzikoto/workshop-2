﻿using System;
using ClassesII_Demo.Models;
using System.Text.Json;
using System.IO;

namespace ClassesII_Demo.Commands
{
    public class SerializeObjectsCommand
    {   static int seed = 3;
        public SerializeObjectsCommand()
        {
        }
        public string Execute(string[] args)
        {
            if (args.ToString() != ("all"))
            {

                //have to find create a file command / for now it will save it in already created file
                //string outputJSON = Newtonsoft.Json.JsonConvert.SerializeObject(Phonebook.ListContacts(), Newtonsoft.Json.Formatting.Indented);
                //File.WriteAllText(@"../../../contacts"+seed.ToString()+".json", outputJSON + Environment.NewLine);
                string outputJSON = Newtonsoft.Json.JsonConvert.SerializeObject(Phonebook.ListContacts(), Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(@"../../../contacts2.json", outputJSON + Environment.NewLine);
                return "contacts" +seed.ToString();
            }
            else
            {
                return $"There are no contacts in the Phonebook. Add some!!!";
            }
           

        }
    }
}
