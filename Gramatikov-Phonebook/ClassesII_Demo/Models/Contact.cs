﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClassesII_Demo.Models
{
    public class Contact
    {
        private static int currentContactId = 1;
        private string name;
        DateTime createdOn = new DateTime(); 
        private string phoneNumber;
        public Contact(string name, string phoneNumber)
        {
            this.Id = currentContactId++;
            this.Name = name;
            this.PhoneNumber = phoneNumber;
            this.CreatedOn = DateTime.Now;
        }

        public int Id { get; set; }

        public string Name
        {
            get { return this.name; }
            set
            {  
                if (value == null  ||( value.Length<=1 && value.Length >= 20))
                {
                    throw new ArgumentException("Product name should contain between 1 and 20 symbols");
                }
                this.name = value;
            }
        }

        public string PhoneNumber
        {
            get { return this.phoneNumber; }
            set
            {
                if (value==null  )
                {
                    throw new ArgumentException("You have not provided a phone Number");

                }
                else if (value.Length != 10)
                {
                    throw new ArgumentException("Phone numbers must be exactly 10 digits");

                }

                this.phoneNumber = value;
            }
        }
        public DateTime CreatedOn { get; set; }

        public string GetContactInfo()
        {
            return $"Id: {this.Id} | {this.Name}: [{this.PhoneNumber}] - Create on {this.CreatedOn}";
        }
    }
}
