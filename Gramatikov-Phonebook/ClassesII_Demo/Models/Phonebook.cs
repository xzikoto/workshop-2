﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ClassesII_Demo.Models
{
    public static class Phonebook
    {
        private static readonly List<Contact> contacts = new List<Contact>();

        public static void SeedContacts()
        {
            if (!contacts.Any())
            {
                string json = File.ReadAllText(@"../../../contacts.json");
                var seededContacts = JsonConvert.DeserializeObject<List<Contact>>(json);

                foreach (var contact in seededContacts)
                {
                    contacts.Add(contact);
                }
            }
        }

        public static void AddContact(Contact contact)
        {
            contacts.Add(contact);

        }
       
        public static void RemoveContact(Contact contact)
        {



            if (contacts.Remove(contact) == true)
            {
                Phonebook.RemoveContact(contact);
            }
            else
            {
                
                throw new ArgumentException("Please provide a contact name");
            }



        }
        public static string RemoveContact(string name)
        {
            Contact contactToRemove = null;
            foreach (var contact in contacts)
            {
                if (contact.Name == name)
                {
                    contactToRemove = contact;
                }
            }

            if (contactToRemove == null)
            {
                return $"Contact {name} does not exist!";
            }

            contacts.Remove(contactToRemove);
            return $"Contact removed";
        }

        public static string UpdateContact(string name, string phoneNumber)
        {

            foreach (var contact in contacts)
            {
                if (contact.Name == name)
                {
                    if (contact.PhoneNumber == phoneNumber == true)
                    {
                        return $"Contact updated";
                    }
                    else
                    {
                        return $"Contact pesho does not exist!";
                    }
                }
            }

            return null;

        }
        public static Contact GetContact(String name, string phoneNumber)
        {
            Contact contact = null;
            foreach (Contact item in contacts)
            {
                if (item.Name == name)
                {
                    if (item.PhoneNumber == phoneNumber)
                    {
                        return contact = (Contact)item;
                    }
                    
                }
            }
            throw new Exception("The contact does not exist");

        }

        public static bool doesItExist(Contact c) {

            foreach (var item in contacts)
            {
                if ((Contact)(item) == c)
                {
                    return true;
                }
            }
            
            return false;
        }

        public static List<string> Search(string startString)
        {
            List<string> mediator = new List<string>();
            for (int i = 0; i < 9; i++)
            {
                if (startString.StartsWith(i.ToString()))
                {
                    mediator.Add("There is no contact name that starts with a digit");
                    return mediator;
                }
            }

            foreach (var contact in contacts)
            {
                if (contact.Name.StartsWith(startString) == true)
                {
                    mediator.Add(contact.ToString());
                }
                else
                {
                    mediator.Add("There is no contact in the Phone book that starts with");
                    return mediator;
                }
            }
            return mediator;
        }
        public static string ListContacts()
        {
            var builder = new StringBuilder();
            
            foreach (var contact in contacts)
            {
                builder.AppendLine(contact.GetContactInfo());
            }

            return builder.ToString();
        }
        //contacts.OrderBy(x => x.Name);
        public static string ListContactsName()
        {
            var builder = new StringBuilder();
            contacts.OrderBy(x => x.Name);
            foreach (var contact in contacts)
            {
                builder.AppendLine(contact.GetContactInfo());
            }

            return builder.ToString();
        }
        public static string ListContactsTime()
        {
            var builder = new StringBuilder();
            contacts.OrderBy(x => x.CreatedOn);
            foreach (var contact in contacts)
            {
                builder.AppendLine(contact.GetContactInfo());
            }

            return builder.ToString();
        }



    }

}
