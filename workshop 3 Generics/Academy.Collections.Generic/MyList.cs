﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Academy.Collections.Generic
{
    public class MyList<T> : IEnumerable<T> , IComparable<T>
    {
        private T[] items;
        //private T type;
        private int length;
        private int counter = 0;
        /// <summary>
        /// Adds an item to the collection.
        /// </summary>
        /// <param name="value">The object to be added to the collection.</param>
        /// <returns>void</returns>
        public MyList()
        {
            this.items = new T[4];

        }

        public void Add(T value)
        {

            if (counter < items.Length)
            {
                this.items[counter] = value;
                counter++;
            }
            else if (counter == this.items.Length)
            {
                T[] temp = this.Resize();
                for (int i = 0; i < this.items.Length; i++)
                {
                    temp[i] = this.items[i];
                }
                this.items = temp;
                this.items[counter] = value;
                counter++;
            }
            this.length = counter;

        }
        public bool Contains(T value)
        {
            foreach (var item in this.items)
            {
                if (item.Equals(value))
                {
                    return true;
                }

            }
            return false;
        }
        public int IndexOf(T item)
        {

            for (int i = 0; i < this.items.Length; i++)
            {
                if (items[i].Equals(item))
                {
                    return i;
                }
            }
            return 0;
        }
        public int LastIndexOf(T item)
        {
            int lastIndex = 0;

            for (int i = 0; i < items.Length; i++)
            {
                if (items[i].Equals(item))
                {
                    lastIndex = i;

                }
            }
            return lastIndex;
        }

        public int Capacity
        {
            get { return this.items.Length; }
        }
        public int Count
        {
            get { return this.length; }
        }

        
        public bool Remove(T value)
        {
            bool removed = false;
            int counter = 0;
            T[] temp = this.ResizeWhenRemove();
            for (int i = 0; i < this.items.Length; i++)
            {
                if (value.Equals(this.items[i]))
                {
                    this.length--;
                    removed = true;
                    for (int j = i + 1; j < this.items.Length; j++)
                    {
                        temp[counter] = this.items[j];
                        counter++;
                    }
                    break;
                }
                else
                {
                    temp[counter] = this.items[i];
                }
                counter++;
            }
            this.items = temp;
            return removed;

        }
        public bool RemoveAt(int value)
        {
            bool removed = false;
            int counter = 0;
            T[] temp = new T[this.items.Length - 1];
            for (int i = 0; i < this.items.Length; i++)
            {
                if (i == value)
                {
                    this.length--;
                    removed = true;
                    for (int j = i + 1; j < this.items.Length; j++)
                    {
                        temp[counter] = this.items[j];
                        counter++;
                    }
                    break;
                }
                else
                {
                    temp[counter] = this.items[i];
                }
                counter++;
            }
            this.items = temp;
            return removed;
        }
        public void Clear()
        {
            for (int i = 0; i < this.Count; i++)
            {
                this.RemoveAt(0);
                i--;
            }

            this.counter = 0;
        }
        public T[] Resize()
        {
            int doubledSize = this.items.Length * 2;
            T[] mediator = new T[doubledSize];
            for (int i = 0; i < doubledSize; i++)
            {
                if (i == this.items.Length - 1)
                {
                    mediator[i] = this.items[i];
                    break;
                }
                mediator[i] = this.items[i];
            }

            return mediator;
        }
        public T[] ResizeWhenRemove()
        {
            T[] mediator = new T[this.items.Length - 1];
            return mediator;

        }
        public void Swap(int a , int b)
        {
            T[] temporary = new T[items.Length] ;

            temporary[a] = items[a];
            temporary[b] = items[b];
            items[a] = temporary[b];
            items[b] = temporary[a];
        }
        public T this[int i]
        {
            get { return items[i]; }
            set { items[i] = value; }
        }
        public IEnumerator<T> GetEnumerator()
        {
            return this.items.Take(this.length).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.items.Take(this.length).GetEnumerator();
        }
        public override string ToString()
        {
            String convertedInfoFromList = "";

            foreach (var item in this.items)
            {
                convertedInfoFromList += " " + item;
            }
            return convertedInfoFromList;
        }

        public int CompareTo([AllowNull] T other)
        {
            throw new NotImplementedException();
        }
        //public void Sort()
        //{
        //    T[] mediatorComparer = new T[this.items.Length];
        //    for (int i = 0; i < this.items.Length; i++)
        //    {
        //        for (int j = 1; j<= this.items.Length; j++)
        //        {
        //            if (this[i].CompareTo()
        //            {
        //                mediatorComparer[i] = this.items[i];
        //            }
        //        }
        //    }
        //    this.items = mediatorComparer;
        //}

        //public int CompareTo([AllowNull] T other)
        //{

        //}
    }
}
