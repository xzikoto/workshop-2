﻿using Academy.Collections.Generic;
using System;


namespace Academy.Collections.Generic.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            MyList<int> allData = new MyList<int>();
            
            
            allData.Add(1);
            allData.Add(6);
            allData.Add(2);
            allData.Add(6);
            allData.Add(3);
            allData.Add(6);
            allData.Add(4);
            allData.Add(6);
            allData.Add(5);
            allData.Add(6);


            //allData.Clear();
            //allData.Sort();
            foreach (var item in allData)
            {
                Console.WriteLine(item);
            }

            //allData.ToString();
            //Console.WriteLine(allData.ToString());

        }
    }
}
