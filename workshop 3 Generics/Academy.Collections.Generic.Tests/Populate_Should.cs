﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Academy.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Collections.Generic.Tests
{
    [TestClass]
    public class Populate_Should
    {
        [TestMethod]
        public void CorrectlyPopulatesList()
        {
            //Arrange
            var list = new MyList<int>();

            //Act
            list.Populate(5);

            //Assert
            Assert.AreEqual(4, list.Count); 
        }
    }
}
