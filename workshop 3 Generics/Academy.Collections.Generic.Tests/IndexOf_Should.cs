﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Academy.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Academy.Collections.Generic.Tests
{
    [TestClass]
    public class IndexOf_Should
    {
        [TestMethod]
        public void ReturnCorrectIndex()
        {
            //Arrange
            var list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);

            //Act
            var result = list.IndexOf(3);

            //Assert
            Assert.AreEqual(2, result);
        }
    }
}
