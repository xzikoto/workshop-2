﻿using Cosmetics.Cart;
using Cosmetics.Common;
using Cosmetics.Contracts;
using Cosmetics.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;


namespace Cosmetics.Core.Engine
{
    public class CosmeticsFactory : ICosmeticsFactory 
    {
        public ICategory CreateCategory(string name)
        {
            Category category = new Category(name);
            //ICategory icategory = (ICategory)category;
            //return icategory;
            if (category as ICategory != null)
            {
                return category;
            }
            else
            {
                return null;
            }

        }

        public Shampoo CreateShampoo(string name, string brand, decimal price, GenderType gender, uint milliliters, UsageType usage)
        {
            Shampoo shampoo = new Shampoo(name, brand, price, gender, Convert.ToInt32(milliliters), usage);
            return shampoo;
        }

        public Toothpaste CreateToothpaste(string name, string brand, decimal price, GenderType gender, IList<string> ingredients)
        {
            Toothpaste toothpaste = new Toothpaste(name, brand, price, gender, string.Join(",",ingredients));
            return toothpaste;
        }
        public Cream CreateCream(string name, string brand, decimal price, GenderType gender, ScentType scent)
        {
            Cream cream = new Cream(name,brand,price,gender,scent);
            return cream;
        }
        public ShoppingCart CreateShoppingCart()
        {
            ShoppingCart cartShoping = new ShoppingCart();
            return cartShoping;
        }

     }
}
