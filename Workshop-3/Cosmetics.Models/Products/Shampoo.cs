﻿using System;
using Cosmetics.Common;
using Cosmetics.Contracts;

namespace Cosmetics.Products
{
    public class Shampoo : Product, IShampoo
    {
        //private string name;
        //private string brand;
        //private decimal price;
        //private GenderType gender;
        //private int milliliters;
        //private UsageType usage;
        public Shampoo(string name, string brand, decimal price, GenderType gender, int milliliters, UsageType everyDay)
            : base(name, brand, price,gender)
        { 
            this.Milliliters = milliliters;
            this.Usage = everyDay;
        }

        public UsageType Usage { get; set; }

        public decimal Milliliters { get; set; }

        public override string Print()
        {
            string printInfo = "";
            printInfo =
                
                $"#{this.Name} {this.Brand} \n" +
                $" #Price {this.Price} \n" +
                $" #Gender {this.Gender} \n" +
                $" #Milliliters {this.Milliliters} \n" +
                $" #Usage {this.Usage} \n" +
                $" ===" ;
            return  base.Print() +  printInfo;
        }

    }
}
