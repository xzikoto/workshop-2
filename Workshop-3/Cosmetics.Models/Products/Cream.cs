﻿using System;
using Cosmetics.Common;
using Cosmetics.Contracts;

namespace Cosmetics.Products
{
    public class Cream : Product, ICream 
    {
        //private string name;
        //private string brand;
        //private decimal price;
        //private GenderType gender;
        private ScentType scent;
        public Cream(string name, string brand, decimal price, GenderType gender, ScentType scent)
            :base(name,brand,price,gender)
        {
            this.Scent = scent;
        }
       
        public ScentType Scent { get => scent; set => scent = value; }

        public override string Name
        {
            get { return this.name; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Not VALID HEALTH");
                }
                else if (value.Length <= 3)
                {
                    throw new ArgumentOutOfRangeException("Name is smaller than min value (3)");
                }
                else if (value.Length >= 15)
                {
                    throw new ArgumentOutOfRangeException("Name is larger than max value (10)");
                }
                this.name = value;

            }
        }
        public override string Brand
        {
            get { return this.brand; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("not null, try another");
                }
                if (value.Length >= 15)
                {
                    throw new ArgumentOutOfRangeException("Brand cannot be more than ten symbols");
                }
                if (value.Length <= 3)
                {
                    throw new ArgumentOutOfRangeException("Brand cannot be less than two symbols");
                }
                //this should be here but cannot run program without it
                this.brand = value;
            }
        }
        public new virtual string Print()
        {
            string printInfo = "";
            printInfo =
                $"#{this.Name} {this.Brand} \n" +
                $" #Price: {this.Price} \n" +
                $" #Gender: {this.Gender} \n" +
                $" #Scent: {this.Scent} \n"  +
                $" ===";
            return base.Print() + printInfo;
        }
    }
}
