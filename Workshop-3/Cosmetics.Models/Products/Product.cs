﻿using System;
using Cosmetics.Common;
using Cosmetics.Contracts;

namespace Cosmetics.Products
{
    public class Product : IProduct
    {
        protected string name;
        protected string brand;
        protected decimal price;
        protected GenderType gender;

        public Product(string name,string brand, decimal price, GenderType gender)
        {
            this.Name = name;
            this.Brand = brand;
            this.Price = price;
            this.Gender = gender;
        }

        public virtual string Name
        {
            get { return this.name; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Not VALID HEALTH");
                }
                else if (value.Length <= 3)
                {
                    throw new ArgumentOutOfRangeException("Name is smaller than min value (3)");
                }
                else if (value.Length >= 10)
                {
                    throw new ArgumentOutOfRangeException("Name is larger than max value (10)");
                }
                this.name = value;

            }
        }

        public virtual string Brand
        {
            get { return this.brand; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("not null, try another");
                }
                if (value.Length >= 10)
                {
                    throw new ArgumentOutOfRangeException("Brand cannot be more than ten symbols");
                }
                if (value.Length <= 2)
                {
                    throw new ArgumentOutOfRangeException("Brand cannot be less than two symbols");
                }
                //this should be here but cannot run program without it
                this.brand = value;
            }
        }

        public  decimal Price
        {
            get { return this.price; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Not null price, please!");
                }
                this.price = value;
            }
        }


        public GenderType Gender { get; set; }

        public virtual string Print()
        {
            return string.Empty;
        }
    }
}
