﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
namespace Cosmetics.Products
{
    public class Toothpaste : Product, IToothpaste
    {
        //private string name;
        //private string brand;
        //private decimal price;
        //private GenderType gender;
        private string ingredients;

        public Toothpaste(string name, string brand, decimal price, GenderType gender, string ingredients)
            : base(name, brand, price, gender)
        {
            if (ingredients == null)
            {
                throw new ArgumentNullException("CANNOT BE NULL");
            }
            else
            {
                this.Ingredients = ingredients;
            }
        }
        public String Ingredients
        {
            get { return this.ingredients; }
            set
            {
                this.ingredients = value.ToString();
            }
        }



        public override string Print()
        {
            string printInfo = "";
            printInfo =
                $"#{this.Name} {this.Brand} \n" +
                $" #Price: {this.Price} \n" +
                $" #Gender: {this.Gender} \n" +
                $" #Milliliters: {this.Gender}\n" +
                $" #Usage: {this.Ingredients} \n" +
                $" ===";
            return base.Print() + printInfo;
        }

    }
}