﻿namespace Cosmetics.Common
{
    public enum ScentType
    {
        lavender,
        vanilla,
        rose
    }
}
