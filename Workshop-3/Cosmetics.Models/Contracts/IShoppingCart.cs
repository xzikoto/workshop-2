﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using System.Collections.Generic;

namespace Cosmetics.Contracts
{
    public interface IShoppingCart
    {
        // Which methods should be here?
        // Write them
        public void AddProduct(IProduct product);
        public void RemoveProduct(IProduct product);
        public bool ContainsProduct(IProduct product);
        public decimal TotalPrice();




    }
}