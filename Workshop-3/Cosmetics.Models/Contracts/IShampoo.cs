﻿namespace Cosmetics.Contracts
{
    using Cosmetics.Common;

    public interface IShampoo : IProduct
    {
        UsageType Usage { get;  }
        decimal Milliliters { get; }

    }
}