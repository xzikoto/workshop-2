﻿
using Cosmetics.Common;

namespace Cosmetics.Contracts
{
    public interface ICream : IProduct
    {
        public ScentType Scent { get; set; }

        
    }
}
