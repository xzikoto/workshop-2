﻿namespace OlympicGames.Core.Contracts
{
    public interface IExecutableCommand
    {
        string Execute();
        
    }
}
