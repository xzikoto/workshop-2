﻿namespace OlympicGames.Core.Contracts
{
    public interface ICommandParser
    {
        IExecutableCommand Parse(string commandLine);
    }
}
