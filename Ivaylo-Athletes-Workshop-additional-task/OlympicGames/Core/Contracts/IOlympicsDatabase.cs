﻿using System.Collections.Generic;

using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Contracts
{
    public interface IOlympicsDatabase
    {
        ICollection<IAthlete> Athletes { get; }
    }
}
