﻿using System.Collections.Generic;

using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Contracts
{
    public interface IAthleticsFactory
    {
        IBoxer CreateBoxer(string firstName, string lastName, string country, string category, int wins, int losses);

        ISprinter CreateSprinter(string firstName, string lastName, string country, IDictionary<string, double> records);
    }
}
