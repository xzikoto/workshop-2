﻿using System;

using OlympicGames.Core.Contracts;
using OlympicGames.Core.Providers;

namespace OlympicGames.Core
{
    public class Engine : IEngine
    {
        private const string Delimiter = "####################";
        private readonly ICommandParser parser;

        static Engine()
        {
            Instance = new Engine();
        }

        private Engine()
        {
            this.parser = new CommandParser();
        }

        public void Run()
        {
            // Loop
            while (true)
            {
                // Read
                string input = Console.ReadLine();

                // Evaluate
                string result = this.Evaluate(input);

                // Print
                Console.WriteLine(result);
            }
        }

        public static IEngine Instance { get; }

        private string Evaluate(string input)
        {
            // Make End Command again
            if (input == "end")
            {
                Environment.Exit(0);
            }

            try
            {
                var command = this.parser.Parse(input);
                var result = command.Execute();
                return $"{result}{Environment.NewLine}{Delimiter}";
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                return $"ERROR: {e.Message}";
            }
        }
    }
}
