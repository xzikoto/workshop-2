﻿using System;
using System.Collections.Generic;

using OlympicGames.Core.Contracts;
using OlympicGames.Olympics.Contracts;
using OlympicGames.Olympics.Enums;
using Olympics.Olympics;

namespace OlympicGames.Core.Factories
{
    public class AthleticsFactory : IAthleticsFactory
    {
        public IBoxer CreateBoxer(string firstName, string lastName, string country, string category, int wins, int losses) {



            BoxingCategory cat = BoxingCategory.FeatherWeight;
            Boxer boxer;
            try
            {
                cat = (BoxingCategory)Enum.Parse(typeof(BoxingCategory), category,true);
      
            }
            catch (Exception ex)
            {
                Console.WriteLine("NOT VALID BOXING CATEGORY!");
                Console.WriteLine(ex.Message);
            }

            boxer = new Boxer(firstName, lastName, country, cat, wins, losses);
            return boxer;

        }

        public ISprinter CreateSprinter(string firstName, string lastName, string country, IDictionary<string, double> records)
        {
            Sprinter sprinter = new Sprinter(firstName, lastName, country, records);

            return sprinter;
        }

    }
}
