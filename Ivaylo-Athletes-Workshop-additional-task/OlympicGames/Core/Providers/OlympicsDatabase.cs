﻿using System.Collections.Generic;

using OlympicGames.Core.Contracts;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Providers
{
    public class OlympicsDatabase : IOlympicsDatabase
    {
        public OlympicsDatabase()
        {
            this.Athletes = new List<IAthlete>();
        }

        public ICollection<IAthlete> Athletes { get; }
    }
}
