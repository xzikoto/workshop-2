﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Commands;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using System.Linq;

namespace OlympicGames.Core.Providers
{
    public class CommandParser : ICommandParser
    {
        // Converts input from the console to an actual instance of ICommand
        public IExecutableCommand Parse(string input)
        {
            // Trim any white spaces
            input = input.Trim();

            // Use a single space in order to convert the input line from string to an array of strings
            string[] inputLineParameters = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            // Get the name of the command (createboxer, createsprinter or listolympians)
            string commandName = inputLineParameters[0];

            // Everything after the name of the command is considered a parameter
            List<string> commandParameters = new List<string>();
            //for (int i = 1; i < inputLineParameters.Length; i++)
            //{
            //    commandParameters.Add(inputLineParameters[i]);
            //}

            commandParameters = inputLineParameters.Skip(1).ToList();
            // TODO: The code above can be done in 1 line.
            // Hint: https://docs.microsoft.com/en-us/dotnet/api/system.linq.enumerable.skip?view=netcore-3.1

            
            // Create a new instance based on the name of the command
           return Command(commandName, commandParameters);
            

            // TODO: The code above uses the classical switch statement. 
            // Try using the new C# 8.0 switch expressions. 
            // Hint: https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#switch-expressions

            
        }
        public IExecutableCommand Command(string commandName, List<string> commandParameters)
        =>
            commandName switch
            {
                "createboxer" => new CreateBoxerCommand(commandParameters),
                "createsprinter" => new CreateSprinterCommand(commandParameters),
                "listolympians" => new ListOlympiansCommand(commandParameters),
                _ => throw new Exception(),
            };
    }
}