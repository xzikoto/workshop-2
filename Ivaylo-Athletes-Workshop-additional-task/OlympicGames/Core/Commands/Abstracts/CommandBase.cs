﻿using System.Collections.Generic;
using System.Linq;
using OlympicGames.Core.Contracts;
using OlympicGames.Core.Factories;
using OlympicGames.Core.Providers;

namespace OlympicGames.Core.Commands.Abstracts
{
    public abstract class CommandBase : IExecutableCommand
    {
        private static IAthleticsFactory factory;
        private static IOlympicsDatabase database;

        // This is a static constructor
        static CommandBase()
        {
            factory = new AthleticsFactory();
            database = new OlympicsDatabase();
        }

        protected CommandBase(IEnumerable<string> parameters)
        {
            // TODO: Which one is better? Write your answer as comment for extra points.
            //this.Parameters = parameters;
            // or
            this.Parameters = new List<string>(parameters);
            
        }

        protected IEnumerable<string> Parameters { get; }
        protected IAthleticsFactory Factory => factory;
        protected IOlympicsDatabase Database => database;

        // TODO: This does not belong here
        public abstract string Execute();
    }
}
