﻿using System;
using System.Collections.Generic;

using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using Olympics.Olympics.Enums;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Commands
{
    public class ListOlympiansCommand : CommandBase
    {
        public ListOlympiansCommand(IEnumerable<string> parameters) 
            : base(parameters)
        {
        }

        public override string Execute()
        {
            var col = new List<IAthlete>(this.Database.Athletes);
            #region reflection
            try
            {
                switch (base.Parameters.Count())
                {
                    case 0:
                        {
                            col = this.Database.Athletes.OrderBy(x => x.Firstname).ToList();
                            
                        }
                        break;
                    case 1:
                        {
                            col = this.Database.Athletes.OrderBy(x =>
                            {
                                return x.GetType()
                               .GetProperties()
                               .FirstOrDefault(y => y.Name.ToLower() == base.Parameters.ToArray()[0].ToLower())
                               .GetValue(x);
                            }).ToList();
                            break;
                        }

                    case 2:
                        {
                            if (base.Parameters.ToArray()[1].ToLower() == "asc")
                            {
                                col = this.Database.Athletes.OrderBy(x =>
                                {
                                    return x.GetType()
                                   .GetProperties()
                                   .FirstOrDefault(y => y.Name.ToLower() == base.Parameters.ToArray()[0].ToLower())
                                   .GetValue(x);
                                }).ToList();

                            }
                            else if (base.Parameters.ToArray()[1].ToLower() == "desc")
                            {
                                col = this.Database.Athletes.OrderBy(x =>
                                {
                                    return x.GetType()
                                   .GetProperties()
                                   .FirstOrDefault(y => y.Name.ToLower() == base.Parameters.ToArray()[0].ToLower())
                                   .GetValue(x);
                                }).ToList();
                                col.Reverse();
                            }

                        }
                        break;
                    default: throw new ArgumentNullException("Not valid input");


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Not a proper command for listolypians [property] [sort:asc/desc]");
                Console.WriteLine(ex.Message);
            }
            
            #endregion

            string sortedAthletes = "";
            foreach (var item in col)
            {
                sortedAthletes += ((Athlete)item).Print()+"\n";
            }
            return sortedAthletes;
            
        }

        
    }
}
