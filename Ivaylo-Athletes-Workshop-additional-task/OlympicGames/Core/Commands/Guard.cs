﻿using System;
using System.Collections.Generic;

namespace Olympics.Core.Commands
{
    public static class Guard
    {
        public static void ValidateParamethersCount(List<string> parametherList, int expected)
        {
            if (parametherList == null)
                throw new ArgumentNullException("List cannot be null!");

            if (parametherList.Count != expected)
                throw new ArgumentException("Not enough parameters for Creating a sprinter!");

        }

        public static int ValidateNumber(string number)
        {
            var success = int.TryParse(number, out int parsedNumber);

            if (!success)
                throw new ArgumentNullException("Wins and Losses should be INTegers");

            return parsedNumber;
        }

    }
}
