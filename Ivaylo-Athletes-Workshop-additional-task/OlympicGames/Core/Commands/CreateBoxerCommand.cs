﻿using System;
using System.Collections.Generic;
using System.Linq;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Olympics.Contracts;
using OlympicGames.Olympics.Enums;
using Olympics.Core.Commands;
using Olympics.Olympics;
using Olympics.Olympics.Enums;

namespace OlympicGames.Core.Commands
{
    public class CreateBoxerCommand : CommandBase
    {
        public CreateBoxerCommand(IEnumerable<string> parameters) 
             : base(parameters) 
        {
        }

        public override string Execute()
        {

            Guard.ValidateParamethersCount(base.Parameters.ToList(), expected: 6);

            int wins = Guard.ValidateNumber(base.Parameters.ToList()[4]);
            int losses = Guard.ValidateNumber(base.Parameters.ToList()[5]);
            string firstName = base.Parameters.ToArray()[0];
            string lastName = base.Parameters.ToArray()[1];
            string country = base.Parameters.ToArray()[2];
            string category = base.Parameters.ToArray()[3];
            

            BoxingCategory cat;
            BoxingCategory validCategory = BoxingCategory.FeatherWeight;
            try
            {
               
                cat = (BoxingCategory)(Enum.Parse(typeof(BoxingCategory), category,true));
                validCategory = cat;

            }
            catch (Exception)
            {
                Console.WriteLine("NOT VALID BOXING CATEGORY!");
            }


            IAthlete boxer = this.Factory.CreateBoxer(firstName,lastName,country,category,wins,losses);
            this.Database.Athletes.Add(boxer);

            
            Console.WriteLine("Created Boxer");
            return ((Boxer)boxer).Print();
        }
       
        
    }
}
