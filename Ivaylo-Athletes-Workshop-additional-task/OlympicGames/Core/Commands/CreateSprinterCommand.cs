﻿using System;
using System.Collections.Generic;
using System.Linq;
using OlympicGames.Core.Commands.Abstracts;
using Olympics.Core.Commands;
using Olympics.Olympics;

namespace OlympicGames.Core.Commands
{
    public class CreateSprinterCommand : CommandBase
    {
        public CreateSprinterCommand(IEnumerable<string> parameters)
            : base(parameters)
        {
        }

        public override string Execute()
        {
          

            string firstName = base.Parameters.ToArray()[0];
            string lastName = base.Parameters.ToArray()[1];
            string country = base.Parameters.ToArray()[2];
            Dictionary<string, double> dic = new Dictionary<string, double>();

            //not a final validation
            Guard.ValidateParamethersCount(base.Parameters.ToList(), base.Parameters.Count());
            //Try to make this again and again

            for (int i = 3; i < base.Parameters.Count(); i++)
            {
                string[] commandLine = base.Parameters.ToArray()[i].Split('/');
                dic.Add(commandLine[0], Convert.ToDouble(commandLine[1]));
            }

            Sprinter sprinter = new Sprinter(firstName, lastName, country, dic);

            base.Database.Athletes.Add(sprinter);
            String records = "";

            foreach (var item in dic)
            {
                records += $"{item.Key}m , {item.Value.ToString()}s \n";
            }
            if (records =="")
            {
                return sprinter.Print() + records + "NO PERSONAL RECORDS SET";

            }
            else
            {
                return sprinter.Print()+ "PERSONAL RECORDS:\n"+records;
            }

        }
    }
}
