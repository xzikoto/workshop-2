﻿using OlympicGames.Core;

namespace OlympicGames
{
    public static class Program
    {
        public static void Main() => Engine.Instance.Run();
        
    }
}
