﻿using System;
using OlympicGames.Olympics.Contracts;

namespace Olympics.Olympics.Enums
{
    public abstract class Athlete : IAthlete
    {
        private string firstName;
        private string lastName;
        private string country;

        public Athlete(string firstName, string lastName, string country)
        {
            this.Firstname = firstName;
            this.LastName = lastName;
            this.Country = country;
        }

        public string Firstname
        {
            get { return this.firstName; }
            private set
            {
                if (value.Length<2 || value.Length > 20)
                {
                    throw new Exception("FIRST Name should be in range [2-20] symbols");
                }
                else
                {
                    this.firstName = value;
                }

            }
            
        }

        public string LastName
        {
            get { return this.lastName; }
            private set
            {
                if (value.Length < 2 || value.Length > 20)
                {
                    throw new Exception("LAST Name should be in range [2-20] symbols");
                }
                this.lastName = value;
            }
        }

        public string Country
        {
            get { return this.country; }
            private set
            {
                if (value.Length < 3 || value.Length > 25)
                {
                    throw new Exception("LAST Name should be in range [2-25] symbols");
                }
                this.country = value;
            }
        }
        public virtual string Print()
        {
            return string.Empty;
        }

    }
}
