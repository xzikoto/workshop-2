﻿namespace OlympicGames.Olympics.Enums
{
    public enum BoxingCategory
    {
        FlyWeight,
        FeatherWeight,
        LightWeight,
        MiddleWeight,
        HeavyWeight
    }
}
