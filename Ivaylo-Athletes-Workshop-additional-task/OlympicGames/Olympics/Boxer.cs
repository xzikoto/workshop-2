﻿using System;
using OlympicGames.Olympics.Contracts;
using OlympicGames.Olympics.Enums;
using Olympics.Olympics.Enums;

namespace Olympics.Olympics
{
    public class Boxer : Athlete, IBoxer
    {
        private int wins;
        private int losses;

        public Boxer(string firstname, string lastname, string country, BoxingCategory category, int wins, int losses)
            : base(firstname, lastname, country)
        {
            this.Wins = wins;
            this.Losses = losses;
            this.Category = category;
        }

        public int Wins
        {
            get { return this.wins; }
            set
            {
                if (this.wins < 0 || this.wins > 100)
                {
                    throw new Exception("Wins are in the range [0-100]");
                }
                this.wins = value;
            }
        }
        public int Losses
        {
            get { return this.losses; }
            set
            {

                if (this.losses < 0 || this.losses > 100)
                {
                    throw new Exception("Wins are in the range [0-100]");
                }
                this.losses = value;
            }
        }
        public BoxingCategory Category{get;set;}

        public override string Print()
        {
            return $"BOXER: {base.Firstname} {this.LastName} from {base.Country} \n" +
                   $"Category: {this.Category} \n" +
                   $"Wins: {this.Wins}\n" +
                   $"Losses: {this.Losses}";
        }
    }
}
