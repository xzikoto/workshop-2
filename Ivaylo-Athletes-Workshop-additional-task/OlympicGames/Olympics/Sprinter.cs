﻿using System;
using System.Collections.Generic;
using OlympicGames.Olympics.Contracts;
using Olympics.Olympics.Enums;

namespace Olympics.Olympics
{
    public class Sprinter : Athlete , ISprinter
    {
        public Sprinter(string firstName, string lastName, string country, IDictionary<string,double> records)
            :base(firstName, lastName,country) 
        {
            this.PersonalRecords = records;

        }


        public IDictionary<string, double> PersonalRecords { get; set; }

        public override string Print()
        {
            return $"SPRINTER: {base.Firstname} {this.LastName} from {base.Country} \n";
                  
        }
    }
}
