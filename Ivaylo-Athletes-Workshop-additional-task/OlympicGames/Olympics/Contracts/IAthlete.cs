﻿namespace OlympicGames.Olympics.Contracts
{
    public interface IAthlete
    {
        string Firstname { get; }
        string LastName { get; }
        public string Country { get; }
    }
}
