﻿using System.Collections.Generic;

namespace OlympicGames.Olympics.Contracts
{
    public interface ISprinter : IAthlete
    {
        IDictionary<string, double> PersonalRecords { get; }
    }
}
