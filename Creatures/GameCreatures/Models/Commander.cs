﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameCreatures.Models
{
    public class Commander
    {
        private readonly List<Creature> army;

        
        private string name;
        public Commander(string name, List<Creature> army)
        {
            this.Name = name;
            
            if (army == null)
            {
                throw new ArgumentNullException("Army could not be null");
            }
            this.army = army;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("NullName");
                }
                else
                {
                    this.name = value;
                }
            }
        }
        public List<Creature> Army
        {
            get { return this.army; }
        }
        public int ArmySize
        {
            get
            {
                return this.army.Count;
            }
            
        }
        public Creature StrongestCreature(List<Creature> creatures)
        {
            int maxDamage = 0;
            foreach (var item in creatures)
            {
                if (item.Damage > maxDamage )
                {
                    maxDamage = item.Damage;
                }
            }
            foreach (var item in creatures)
            {
                if (maxDamage == item.Damage)
                {
                    return item;
                }
            }
            return null;
        }
        public Creature GetCreature(int index , Commander commander)
        {
            Creature creature = null;
            for (int i = 0; i < commander.Army.Count; i++)
            {
                if (index == i)
                {
                    return creature = commander.Army[i];
                }
            }
            return null;
            
        }

        public void AttackAtPosition(Commander enemy, int attackerIndex, int targetIndex)
        {
            //Creature friednly = GetCreature(attackerIndex,this);
            //Creature target = GetCreature(targetIndex, enemy);

            army[attackerIndex].Attack(enemy.army[targetIndex]);

            if (enemy.army[targetIndex].HealthPoints <= 0)
            {
                enemy.army.RemoveAt(targetIndex);
            }
           
           

        }

        public void AutoAttack(Commander enemy)
        {
            List<Creature> attackerSorted = new List<Creature>();

            //Creature c = null;

            if (enemy.army.Count == 1)
            {
                attackerSorted = this.army.OrderByDescending(c => c.CalculateActualDamage(enemy.army[0])).ToList();
                attackerSorted[0].Attack(enemy.army[0]);

                if (enemy.army[0].HealthPoints <= 0)
                {
                    enemy.army.Remove(enemy.army[0]);
                }
            }
            else
            {
                attackerSorted = this.army.OrderByDescending(c => c.CalculateActualDamage(c.FindBestTarget(enemy.army))).ToList();


                attackerSorted[0].AutoAttack(enemy.army);
            }
        }
    }
}
