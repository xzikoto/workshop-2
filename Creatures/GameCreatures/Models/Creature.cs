﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace GameCreatures.Models
{
    public class Creature
    {
        private string name;
        private int damage;
        private int healthPoints;

        

        public Creature(
            string name,
            int damage,
            int healthPoints,
            AttackType attackType,
            ArmorType armorType)
        {
            this.Name = name;
            this.Damage = damage;
            if (healthPoints <= 0)
            {
                throw new ArgumentOutOfRangeException("Not VALID HEALTH");
            }
            this.HealthPoints = healthPoints;
            this.AttackType = attackType;
            this.ArmorType = armorType;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Could not be Null or Empty string");
                }
                else
                {
                    this.name = value;
                }
                
            }
        }

        public int Damage
        {
            get
            {
                return this.damage;
            }
            set
            {
                
                if (value == 0 )
                {
                    throw new ArgumentOutOfRangeException("Not a valid damage");
                }
                
                this.damage = value;
                
            }
        }

        public int HealthPoints
        {
            get
            {

                return this.healthPoints;
            }
            set
            {

                if (value < 0)
                {
                    value = 0;
                }

                this.healthPoints = value;
                

            }
        }

        public ArmorType ArmorType { get;  }

        public AttackType AttackType { get;  }


        public void Attack(Creature target)
        {
            //this.Attack(target);

            target.HealthPoints -= this.CalculateActualDamage(target);

        }
        
        public Creature FindBestTarget(List<Creature> targets)
        {
            List<Creature> creaturesThatDie = new List<Creature>();

            List<Creature> crTDSorted = new List<Creature>();

            List<Creature> creaturesThatDontDie = targets;

            List<Creature> crTDDSorted = new List<Creature>();

            bool isDead = true;
            foreach (var c in targets)
            {
                if (c.HealthPoints - CalculateActualDamage(c) <= 0)
                {
                    creaturesThatDie.Add(c);
                }
            }
            if (creaturesThatDie.Count == 0)
            {
                isDead = false;

                crTDDSorted = creaturesThatDontDie.OrderBy(c => c.HealthPoints - CalculateActualDamage(c)).ToList();

            }
            if (isDead == false)
            {
                return crTDDSorted[0];
            }
            else
            {
                crTDSorted = creaturesThatDie.OrderByDescending(c => c.Damage).ToList();
                return crTDSorted[0];
            }
        }

        public void AutoAttack(List<Creature> targets)
        {
            Creature c = FindBestTarget(targets);
            c.HealthPoints -= CalculateActualDamage(c);
            if (c.HealthPoints <= 0 )
            {
                targets.Remove(c);
            }
            
        }


        public int CalculateActualDamage(Creature target)
        {
            double actualDamage = this.damage;
            if (this.AttackType == AttackType.Ranged && target.ArmorType == ArmorType.Light)
            {
                actualDamage *= 1.25;

            }
            else if (this.AttackType == AttackType.Melee && target.ArmorType == ArmorType.Light)
            {
                actualDamage *= 1.00;

            }
            else if (this.AttackType == AttackType.Magic && target.ArmorType == ArmorType.Light)
            {
                actualDamage *= 0.75;

            }
            else if (this.AttackType == AttackType.Ranged && target.ArmorType == ArmorType.Medium)
            {
                actualDamage *= 1.00;

            }
            else if (this.AttackType == AttackType.Melee && target.ArmorType == ArmorType.Medium)
            {
                actualDamage *= 1.25;
            }
            else if (this.AttackType == AttackType.Magic && target.ArmorType == ArmorType.Medium)
            {
                actualDamage *= 1.00;
            }

            else if (this.AttackType == AttackType.Ranged && target.ArmorType == ArmorType.Heavy)
            {
                actualDamage *= 0.75;
            }
            else if (this.AttackType == AttackType.Melee && target.ArmorType == ArmorType.Heavy)
            {
                actualDamage *= 0.75;
            }
            else if (this.AttackType == AttackType.Magic && target.ArmorType == ArmorType.Heavy)
            {
                actualDamage *= 1.25;
            }
            return (int)Math.Floor(actualDamage);
        }
    }
}
